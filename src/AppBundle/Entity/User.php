<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * User
 *
 * @ORM\Table(name="user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @UniqueEntity("email", message="Данная почта уже занята")
 * @UniqueEntity("username", message="Данный логин уже занят")
 */
class User extends BaseUser
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Поле логин не должно быть пустым")
     * @Assert\Type(type="string", message="Поле логин должно содержать только строковые значения")
     * @Assert\Length(
     *     max=128,
     *     maxMessage="Длина логина не должна превышать {{ limit }} символов"
     * )
     */
    protected $username;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Поле email не должно быть пустым")
     * @Assert\Email(message="Введен неправильный формат электронной почты")
     */
    protected $email;

    /**
     * @var string
     *
     * @Assert\NotBlank(message="Пароль не должен быть пустым")
     * @Assert\Type(type="string", message="Пароль должен содержать только строковые значения")
     * @Assert\Length(
     *     min=6,
     *     minMessage="Пароль не должен быть меньше {{ limit }} символов",
     *     max=128,
     *     maxMessage="Пароль не должен превышать {{ limit }} символов"
     * )
     */
    protected $plainPassword;

    /**
     * @var Post[]|ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Post", mappedBy="user")
     */
    private $posts;

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", mappedBy="follows")
     */
    private $followers;

    /**
     * @var User[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="followers")
     * @ORM\JoinTable(name="followers",
     *     joinColumns={@ORM\JoinColumn(name="follower_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="followed_id", referencedColumnName="id")}
     * )
     */
    private $follows;

    /**
     * @var Post[]|ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Post", mappedBy="likers")
     */
    private $likedPosts;


    public function __construct()
    {
        parent::__construct();
        $this->posts = new ArrayCollection();
        $this->followers = new ArrayCollection();
        $this->follows = new ArrayCollection();
        $this->likedPosts = new ArrayCollection();
    }

    /**
     * @return Post[]|ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @return User[]|ArrayCollection
     */
    public function getFollows()
    {
        return $this->follows;
    }

    /**
     * @param User $user
     */
    public function addFollower(User $user)
    {
        $this->followers->add($user);
    }

    /**
     * @param User $user
     */
    public function addFollow(User $user)
    {
        $this->follows->add($user);
    }

    /**
     * @param User $user
     */
    public function removeFollow(User $user)
    {
        $this->follows->removeElement($user);
    }

    /**
     * @param User $user
     */
    public function removeFollower(User $user)
    {
        $this->followers->removeElement($user);
    }

    /**
     * @param User $user
     */
    public function hasFollow(User $user)
    {
        $this->follows->contains($user);
    }

    /**
     * @return Post[]|ArrayCollection
     */
    public function getLikedPosts()
    {
        return $this->likedPosts;
    }

    /**
     * @param Post $post
     */
    public function addLikedPost(Post $post)
    {
        $this->likedPosts->add($post);
    }

    /**
     * @param Post $post
     */
    public function removeLikedPost(Post $post)
    {
        $this->likedPosts->removeElement($post);
    }
}

