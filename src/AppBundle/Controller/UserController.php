<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/user")
 *
 * Class UserController
 * @package AppBundle\Controller
 */
class UserController extends Controller
{
    /**
     * @Route("/{id}/cancel-follow", name="user_cancel_follow", requirements={"id" : "\d+"})
     *
     * @param User $followUser
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function cancelFollowAction(User $followUser, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $user->removeFollow($followUser);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('main_index');
    }

    /**
     * @Route("/{id}/follow", name="user_follow", requirements={"id" : "\d+"})
     *
     * @param User $followUser
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function followAction(User $followUser, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $user->addFollow($followUser);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('main_index');
    }
}
