<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostDeleteType;
use AppBundle\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MainController extends Controller
{
    /**
     * @Route("/", name="main_index")
     * @Method("GET")
     *
     * @return Response
     */
    public function indexAction(): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        $follows = $user->getFollows();

        $followPosts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->getLatestPostsByFollows($follows);

        $otherPosts = $this->getDoctrine()
            ->getRepository(Post::class)
            ->getLatestPostsOthers($follows, $user);

        return $this->render('@App/Main/index.html.twig', [
            'follow_posts' => $followPosts,
            'other_posts' => $otherPosts
        ]);
    }

    /**
     * @Route("/profile", name="main_profile")
     * @Method("GET")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function profileAction(Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $post = new Post();
        $form = $this->createForm(PostType::class, $post, [
            'action' => $this->generateUrl('post_create'),
            'method' => 'POST'
        ]);

        $myPosts = $em->getRepository(Post::class)->findBy(['user' => $user], ['createdAt' => 'desc']);
        $postDeleteForm = $this->createForm(PostDeleteType::class);

        return $this->render('@App/Main/profile.html.twig', [
            'user' => $user,
            'my_posts' => $myPosts,
            'form' => $form->createView(),
            'post_delete_form' => $postDeleteForm
        ]);
    }

}
