<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Post;
use AppBundle\Entity\User;
use AppBundle\Form\PostDeleteType;
use AppBundle\Form\PostType;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/post")
 *
 * Class PostController
 * @package AppBundle\Controller
 */
class PostController extends Controller
{
    /**
     * @Route("/create", name="post_create")
     * @Method("POST")
     *
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function createAction(Request $request, EntityManagerInterface $em): Response
    {
        /** @var User $user */
        $user = $this->getUser();

        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        $myPosts = $em->getRepository(Post::class)->findBy(['user' => $user], ['createdAt' => 'desc']);
        $postDeleteForm = $this->createForm(PostDeleteType::class);

        if ($form->isSubmitted() && $form->isValid()) {
            $post->setUser($user);
            $em->persist($post);
            $em->flush();

            return $this->redirectToRoute('main_profile');
        }

        return $this->render('@App/Main/profile.html.twig', [
            'user' => $user,
            'my_posts' => $myPosts,
            'form' => $form->createView(),
            'post_delete_form' => $postDeleteForm
        ]);
    }

    /**
     * @Route("/{id}/delete", name="post_delete", requirements={"id" : "\d+"})
     * @Method("DELETE")
     *
     * @param Post $post
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function deleteAction(Post $post, EntityManagerInterface $em): Response
    {
        $em->remove($post);
        $em->flush();

        return $this->redirectToRoute('main_profile');
    }

    /**
     * @Route("{id}/like", name="post_like", requirements={"id" : "\d+"})
     *
     * @param Post $post
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function likeAction(Post $post, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();

        if (!$post->hasLiker($user)) {
            $post->addLiker($user);
            $em->persist($post);
            $em->flush();
        }

        return $this->redirectToRoute('main_index');
    }

    /**
     * @Route("{id}/unlike", name="post_unlike", requirements={"id" : "\d+"})
     *
     * @param Post $post
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function unlikeAction(Post $post, EntityManagerInterface $em): Response
    {
        $user = $this->getUser();

        if ($post->hasLiker($user)) {
            $post->removeLiker($user);
            $em->persist($post);
            $em->flush();
        }

        return $this->redirectToRoute('main_index');
    }
}
